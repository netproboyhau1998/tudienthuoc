package com.thuannluit.tudienthuoc.controller;


import com.thuannluit.tudienthuoc.model.BenhVien;
import com.thuannluit.tudienthuoc.services.BenhVienService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BenhVienController {
    BenhVienService benhVienService;
    public BenhVienController(BenhVienService benhVienService) {
        this.benhVienService = benhVienService;
    }
    @PostMapping(value = "/ggmap/{id}")
    public BenhVien getLocationBenhVien(@PathVariable(value = "id") int id) {
        return benhVienService.getLocationBenhVien(id);
    }
}
