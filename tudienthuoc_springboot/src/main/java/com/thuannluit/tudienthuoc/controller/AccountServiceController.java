package com.thuannluit.tudienthuoc.controller;

import com.thuannluit.tudienthuoc.model.Account;
import com.thuannluit.tudienthuoc.services.AccountNotFoundException;
import com.thuannluit.tudienthuoc.services.AccountService;
import com.thuannluit.tudienthuoc.utils.SendEmail;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class AccountServiceController {
    private final AccountService accountService;

    public AccountServiceController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = "/dangky")
    public Account create(@RequestBody Account account) {
        return accountService.createAccount(account);
    }

    @GetMapping(value = "/dangnhap/{email}/{pass}")
    public Account loginAccount(@PathVariable(value = "email") String email, @PathVariable(value = "pass") String pass) {
        return accountService.login(email, pass);
    }

    @GetMapping(value = "/taikhoan")
    public List<Account> getAllAccount() {
        return accountService.getAllAccount();
    }

    @GetMapping(value = "/taikhoan/{email}/{pass}")
    public Account getInformationAccount(@PathVariable(value = "email") String email, @PathVariable(value = "pass") String pass) {
        return accountService.getInformationAccount(email, pass);
    }

    @PostMapping(value = "/thaydoimatkhau")
    public Account forgetPassword(@RequestBody Account account) throws AccountNotFoundException {
        return accountService.updatePassword(account);
    }
    @PostMapping(value = "/laylaimatkhau/{email}")
    public String getPassAccount(@PathVariable(value = "email") String email) {
        Account account = accountService.getPassAccount(email);
        if(account != null) {
            SendEmail.send(email, "Gửi password thành công", account.getAccount_pass());
            return "thành công";
        }
        return "thất bại";
    }

    @PostMapping(value = "/capnhattaikhoan/{id}")
    public Account updateAccount(@PathVariable(value = "id") Integer id, @RequestBody Account account) throws AccountNotFoundException {
        return accountService.updateAccount(id, account);
    }

    @GetMapping(value = "/kiemtraemail/{email}")
    public String isEmailExist(@PathVariable(value = "email") String email) {
        return accountService.isEmailExist(email);
    }
}
