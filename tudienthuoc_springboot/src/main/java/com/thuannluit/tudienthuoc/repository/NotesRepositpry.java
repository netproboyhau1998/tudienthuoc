package com.thuannluit.tudienthuoc.repository;

import com.thuannluit.tudienthuoc.model.Notes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotesRepositpry extends JpaRepository<Notes, Integer> {
}
