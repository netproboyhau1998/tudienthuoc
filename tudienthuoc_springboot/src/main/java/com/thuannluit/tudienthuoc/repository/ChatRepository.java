package com.thuannluit.tudienthuoc.repository;

import com.thuannluit.tudienthuoc.model.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ChatRepository extends JpaRepository<ChatMessage, Integer> {


}
