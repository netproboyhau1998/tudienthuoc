package com.thuannluit.tudienthuoc.services;

import com.thuannluit.tudienthuoc.model.Notes;
import com.thuannluit.tudienthuoc.repository.NotesRepositpry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NotesService {

    @Autowired
    private NotesRepositpry notesRepositpry;

    // lay toan bo ghi chu hien co trong database
    public List<Notes> getAllNotes() {
        return notesRepositpry.findAll();
    }

    // lay toan bo ghi chu theo account id
    public List<Notes> getAllNotesByIdAccount(int id_account) {
        List<Notes> list_all_notes = getAllNotes();
        List<Notes> list_account_notes = new ArrayList<>();
        for (Notes notes : list_all_notes) {
            if (notes.getId_account() == id_account) {
                list_account_notes.add(notes);
            }
        }
        return list_account_notes;
    }

    // tao moi 1 notes
    public Notes createNotes(Notes notes) {
        return notesRepositpry.save(notes);
    }

    // cap nhat 1 notes
    public Notes updateNotes(int id_note, Notes notes) throws NotesNotFoundException {
        Notes notes2 = notesRepositpry.findById(id_note).orElseThrow(() -> new NotesNotFoundException(id_note));
        notes2.setContent_notes(notes2.getContent_notes());
        return notes2;
    }

    // xoa notes
    public void deleteNotes(int id_note){
        System.out.println("Xóa thành công");
        notesRepositpry.deleteById(id_note);
    }

}
