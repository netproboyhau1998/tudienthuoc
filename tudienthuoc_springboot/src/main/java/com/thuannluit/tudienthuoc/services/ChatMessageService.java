package com.thuannluit.tudienthuoc.services;

import com.thuannluit.tudienthuoc.model.ChatMessage;
import com.thuannluit.tudienthuoc.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import java.util.List;

@Service
public class ChatMessageService {

    @Autowired
    private ChatRepository chatRepository;

    // lay toan bo doan hoi thoai
    public List<ChatMessage> getAllHistoryMessage() {
        return chatRepository.findAll();
    }

    // tao 1 cau chat
    public ChatMessage create_message(ChatMessage chatMessage) {
        return chatRepository.save(chatMessage);
    }

    // lay noi dung cau chat moi nhat cua 1 tai khoan
    public ChatMessage get_message(int roomchat_id, int account_id) {
        // lay toan bo tin nhan trong bang chat
        ArrayList<ChatMessage> list_mess = (ArrayList<ChatMessage>) chatRepository.findAll();
        // list mess cua user nay trong phong nay
        ArrayList<ChatMessage> list_mess_user = new ArrayList<>();
        //Tin nhan don le
        ChatMessage chatMessage2 = null;
        // tim nhung tin nhan cua tai khoan nay trong phong nay
        for (ChatMessage chatMessage : list_mess) {
            if (chatMessage.getRoomchat_id() == roomchat_id && chatMessage.getAccount_id() == account_id) {

                list_mess_user.add(
                        new ChatMessage(chatMessage.getRoomchat_id()
                                , chatMessage.getRoomchat_time()
                                , chatMessage.getAccount_id()
                                , chatMessage.getContent_chat()));
            }
        }
        return list_mess_user.get(list_mess_user.size() - 1);
    }

    // lay noi dung doan hoi thoai trong 1 phong cua 2 tai khoan
    public List<ChatMessage> find_chat_history_by_room_id(int room_id, int user_id_1, int user_id_2) {
        // chua danh sach hoi thoai phong chat can tim
        List<ChatMessage> list_chat_history = new ArrayList<>();
        // lay toan bo tin nhan trong bang chat ko phan biet phong
        List<ChatMessage> list_mess = chatRepository.findAll();

        for (ChatMessage chatMessage : list_mess) {
            if (chatMessage.getRoomchat_id() == room_id
                    && chatMessage.getAccount_id() == user_id_1
                    || chatMessage.getAccount_id() == user_id_2) {
                list_chat_history.add(chatMessage);
            }
        }

        return list_chat_history;
    }

}
