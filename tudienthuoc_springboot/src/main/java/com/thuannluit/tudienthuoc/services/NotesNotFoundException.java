package com.thuannluit.tudienthuoc.services;

public class NotesNotFoundException extends Exception {
    public NotesNotFoundException(Integer id) {
        super(String.format("Notes is not found with id : '%s'", id));
    }
}
