package com.thuannluit.tudienthuoc.services;

public class AccountNotFoundException extends Exception  {
    public AccountNotFoundException(Integer id) {
        super(String.format("Account is not found with id : '%s'", id));
    }
}
