package com.thuannluit.tudienthuoc.model;

import javax.persistence.*;

@Entity
@Table(name = "chat", schema = "tudienthuoc")
public class ChatMessage {

    private int chat_id;
    private int roomchat_id;
    private String roomchat_time;
    private int account_id;
    private String content_chat;

    public ChatMessage() {

    }

    public ChatMessage(int roomchat_id, String roomchat_time, int account_id, String content_chat) {
        this.roomchat_id = roomchat_id;
        this.roomchat_time = roomchat_time;
        this.account_id = account_id;
        this.content_chat = content_chat;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "chat_id")
    public int getChat_id() {
        return chat_id;
    }

    public void setChat_id(int chat_id) {
        this.chat_id = chat_id;
    }

    @Basic
    @Column(name = "roomchat_id")
    public int getRoomchat_id() {
        return roomchat_id;
    }

    public void setRoomchat_id(int roomchat_id) {
        this.roomchat_id = roomchat_id;
    }

    @Basic
    @Column(name = "roomchat_time")
    public String getRoomchat_time() {
        return roomchat_time;
    }

    public void setRoomchat_time(String roomchat_time) {
        this.roomchat_time = roomchat_time;
    }

    @Basic
    @Column(name = "account_id")
    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    @Basic
    @Column(name = "content_chat")
    public String getContent_chat() {
        return content_chat;
    }

    public void setContent_chat(String content_chat) {
        this.content_chat = content_chat;
    }

}
