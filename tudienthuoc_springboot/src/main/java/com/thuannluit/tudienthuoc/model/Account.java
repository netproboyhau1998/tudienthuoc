package com.thuannluit.tudienthuoc.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "taikhoan", schema = "tudienthuoc")
public class Account implements Serializable {

    private int account_id;
    private String account_email, account_pass, account_name, account_gender, account_img;

    public Account() {
    }

    public Account(int account_id, String account_email, String account_pass, String account_name, String account_gender, String account_img) {
        this.account_id = account_id;
        this.account_email = account_email;
        this.account_pass = account_pass;
        this.account_name = account_name;
        this.account_gender = account_gender;
        this.account_img = account_img;
    }

    public Account(String account_email, String account_pass) {
        this.account_email = account_email;
        this.account_pass = account_pass;
    }

    @Id
    @Column(name = "account_id")
    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    @Basic
    @Column(name = "account_email")
    public String getAccount_email() {
        return account_email;
    }

    public void setAccount_email(String account_email) {
        this.account_email = account_email;
    }

    @Basic
    @Column(name = "account_pass")
    public String getAccount_pass() {
        return account_pass;
    }

    public void setAccount_pass(String account_pass) {
        this.account_pass = account_pass;
    }

    @Basic
    @Column(name = "account_name")
    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    @Basic
    @Column(name = "account_gender")
    public String getAccount_gender() {
        return account_gender;
    }

    public void setAccount_gender(String account_gender) {
        this.account_gender = account_gender;
    }

    @Basic
    @Column(name = "account_img")
    public String getAccount_img() {
        return account_img;
    }

    public void setAccount_img(String account_img) {
        this.account_img = account_img;
    }

    @Override
    public String toString() {
        return "Account{" +
                "account_id=" + account_id +
                ", account_email='" + account_email + '\'' +
                ", account_pass='" + account_pass + '\'' +
                ", account_name='" + account_name + '\'' +
                ", account_gender='" + account_gender + '\'' +
                ", account_img='" + account_img + '\'' +
                '}';
    }
}
