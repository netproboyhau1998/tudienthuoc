package com.tudien.tudienthuoc.user;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.tudien.tudienthuoc.R;
import com.tudien.tudienthuoc.controller.iDialog;
import com.tudien.tudienthuoc.database.DbAssetBookmark;
import com.tudien.tudienthuoc.model.Account;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuenMatKhauDialog extends DialogFragment implements iDialog {
    Button btnGui, btnThoat;
    EditText email, pass, repass;
    RequestQueue requestQueue;
    Activity activity;
    StringRequest request;
    public static int ID_USER;
    static final String URL = "http://192.168.0.103:8080/laylaimatkhau";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_dialog_quenmatkhau, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        btnGui = (Button) view.findViewById(R.id.btn_guimk);
        btnThoat = (Button) view.findViewById(R.id.btn_thoatqmk);
        email = (EditText) view.findViewById(R.id.nhapemail);
//        pass = (EditText) view.findViewById(R.id.nhapmatkhau);
//        repass = (EditText) view.findViewById(R.id.nhaplaimatkhau);
        requestQueue = Volley.newRequestQueue(activity);

        btnGui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String txtEmail = email.getText().toString().trim().toLowerCase();
//                final String txtPass = pass.getText().toString().trim().toLowerCase();
//                final String txtRepass = repass.getText().toString().trim().toLowerCase();

                if (checkInput(txtEmail) == false) {
                    Toast.makeText(activity, "Kiểm tra lại dữ liệu nhập....", Toast.LENGTH_LONG).show();
                } else {
                    final ProgressDialog progressDialog = new ProgressDialog(activity);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setMessage("Đang gửi...");
                    progressDialog.show();

//                    JSONObject js = new JSONObject();
//                    try {
//                        js.put("account_email", txtEmail);
////                      js.put("account_pass", txtPass);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }

                    request = new StringRequest(Request.Method.POST, URL+"/"+txtEmail, new Response.Listener<String>() {

                        public void onResponse(String response) {
                            try {

                                if (response.equals("thành công")) {
                                    Toast.makeText(activity, "thành công!", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                } else if (response.equals("thất bại")) {
                                    Toast.makeText(activity, "thất bại!", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                }
//                                if (response!= null) {
//                                    DbAssetBookmark assetBookmark = new DbAssetBookmark(activity);
//                                    Account account = new Account((QuenMatKhauDialog.ID_USER = response.getInt("account_id")), response.getString("account_email"),
//                                            response.getString("account_pass"), response.getString("account_name"),
//                                            response.getString("account_gender"), response.getString("account_img"));
//                                    assetBookmark.insertUser(account);
//                                    Toast.makeText(activity, response.getString("account_email"), Toast.LENGTH_LONG).show();
//                                    String email = response;
//                                    Toast.makeText(activity, email, Toast.LENGTH_LONG).show();
//                                    progressDialog.dismiss();

//                                }
//                            else {
//                                    Toast.makeText(activity, "Error !", Toast.LENGTH_LONG).show();
////                                    progressDialog.dismiss();
//                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(activity, "Kết nối mạng không ổn định", Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            headers.put("Content-Type", "application/json; charset=utf-8");
                            return headers;
                        }
                    };
                    requestQueue.add(request);
                }
            }
        });

        btnThoat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }

    private boolean checkInput(String email) {
        if (email.isEmpty() || isValidEmailAddress(email) == false) {
            return false;
        }
        return true;
    }

//    private boolean comparePassword(String pass, String pass2) {
//        if (pass.equalsIgnoreCase(pass2)) {
//            return true;
//        }
//        return false;
//    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void show() {
        this.show(activity.getFragmentManager(), null);
    }
}
