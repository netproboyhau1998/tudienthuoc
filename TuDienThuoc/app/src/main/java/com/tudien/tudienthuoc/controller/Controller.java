package com.tudien.tudienthuoc.controller;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tudien.tudienthuoc.MainActivity;
import com.tudien.tudienthuoc.SendMailActivity;
import com.tudien.tudienthuoc.database.DbAssetBenh;
import com.tudien.tudienthuoc.database.DbAssetBookmark;
import com.tudien.tudienthuoc.database.DbAssetQuanHuyenTinhThanh;
import com.tudien.tudienthuoc.database.DbAssetThuoc;

import org.json.JSONException;
import org.json.JSONObject;

// lop nay tuong lai se lam chuc nang cap nhat phien ban moi cho app

public class Controller {
    private Activity activity;

    public Controller(Activity activity) {
        this.activity = activity;
    }

    public void sendMail() {
        Intent intent = new Intent(activity, SendMailActivity.class);
        activity.startActivity(intent);
    }

    public void capNhatDuLieu() {
        DbAssetThuoc dbAssetThuoc = new DbAssetThuoc(activity);
        DbAssetBenh dbAssetBenh = new DbAssetBenh(activity);
        DbAssetBookmark assetBookmark = new DbAssetBookmark(activity);
        DbAssetQuanHuyenTinhThanh thanh = new DbAssetQuanHuyenTinhThanh(activity);
    }

    public void goMainActivity() {
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }

    public void goActivity(Class lop) {
        Intent intent = new Intent(activity, lop);
        activity.startActivity(intent);
    }

    public void share() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "https://www.facebook.com";
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        activity.startActivity(Intent.createChooser(sharingIntent, "Chọn phương tiện chia sẻ"));
    }

    public void goLike(TabLayout tabLayout) {
        tabLayout.getTabAt(1).select();
    }

    public void initShowDialog(iDialog iDialog) {
        iDialog.setActivity(activity);
        iDialog.show();

    }
}
