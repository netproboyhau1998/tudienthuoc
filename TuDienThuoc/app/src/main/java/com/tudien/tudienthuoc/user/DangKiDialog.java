package com.tudien.tudienthuoc.user;


import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tudien.tudienthuoc.MainActivity;
import com.tudien.tudienthuoc.R;
import com.tudien.tudienthuoc.controller.iDialog;
import com.tudien.tudienthuoc.database.DbAssetBookmark;
import com.tudien.tudienthuoc.model.Account;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DangKiDialog extends DialogFragment implements iDialog {
    EditText name, email, pass, rePass;
    RadioGroup sex;
    String gioiTinh = "Nam";
    RadioButton r_nam, r_nu;
    RequestQueue requestQueue;
    JsonObjectRequest request;

    Button btk_dk, btn_thoat;
    final static String URL = "http://192.168.0.103:8080/dangky";
    final String URL_DN1 = "http://192.168.0.103:8080/kiemtraemail/";
    static final String URL_DN = "http://192.168.0.103:8080/dangnhap/";
    Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_dangki, container, false);
        getDialog().getWindow().setLayout(300, 500);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setTitle("Đăng kí");
        r_nam = (RadioButton) view.findViewById(R.id.r_nam);
        r_nu = (RadioButton) view.findViewById(R.id.r_nu);
        r_nam.setChecked(true);
        r_nam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gioiTinh = r_nam.getText().toString();
                    r_nu.setChecked(false);
                }
            }
        });
        r_nu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gioiTinh = r_nu.getText().toString();
                    r_nam.setChecked(false);
                }
            }
        });
        sex = (RadioGroup) view.findViewById(R.id.radio_group);
        anhXa(view);
        return view;
    }

    public void anhXa(View view) {


        name = (EditText) view.findViewById(R.id.dk_name);
        email = (EditText) view.findViewById(R.id.dk_email);
        pass = (EditText) view.findViewById(R.id.dk_matkhau);
        rePass = (EditText) view.findViewById(R.id.dk_RmatKhau);
        btn_thoat = (Button) view.findViewById(R.id.btn_thoatUp);
        btk_dk = (Button) view.findViewById(R.id.btn_DK);
        requestQueue = Volley.newRequestQueue(activity);


        btk_dk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String txtEmail = email.getText().toString().trim().toLowerCase();
                final String txtPass = pass.getText().toString().trim().toLowerCase();
                final String txtRepass = rePass.getText().toString().trim().toLowerCase();
                final String txtName = name.getText().toString().trim().toLowerCase();
                if (checkInput(txtName, txtEmail, txtPass, txtRepass)) {
                    checkMailExists(txtEmail);
                } else {
                    Toast.makeText(activity, "Nhập thông tin tài khoản!", Toast.LENGTH_LONG).show();
                }
            }
        });
        btn_thoat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private boolean checkInput(String name, String email, String pass, String pass2) {
        if (name.isEmpty() || email.isEmpty() || pass.isEmpty() || pass2.isEmpty() || comparePassword(pass, pass2) == false || isValidEmailAddress(email) == false) {
            return false;
        }
        return true;
    }

    private boolean comparePassword(String pass, String pass2) {
        if (pass.equalsIgnoreCase(pass2)) {
            return true;
        }
        return false;
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public void dangKi() {

        final String txtEmail = email.getText().toString().trim().toLowerCase();
        final String txtPass = pass.getText().toString().trim().toLowerCase();
        final String txtRepass = rePass.getText().toString().trim().toLowerCase();
        final String txtName = name.getText().toString().trim().toLowerCase();


        if (checkInput(txtName, txtEmail, txtPass, txtRepass)) {
            Toast.makeText(activity, "Đang đăng kí...", Toast.LENGTH_SHORT).show();

            JSONObject js = new JSONObject();
            try {
                js.put("account_email", txtEmail);
                js.put("account_pass", txtPass);
                js.put("account_name", txtName);
                js.put("account_gender", gioiTinh);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            request = new JsonObjectRequest(Request.Method.POST, URL, js, new Response.Listener<JSONObject>() {

                public void onResponse(JSONObject response) {
                    try {

                        if (response != null) {
                            Toast.makeText(activity, response.getString("account_email")
                                            + "\n" + response.getString("account_pass")
                                            + "\n" + response.getString("account_name")
                                            + "\n" + response.getString("account_gender")
                                    , Toast.LENGTH_LONG).show();

                            DbAssetBookmark assetBookmark = new DbAssetBookmark(activity);
                            Account account = new Account((DangNhapDialog.ID_USER = response.getInt("account_id")), response.getString("account_email"),
                                    response.getString("account_pass"), response.getString("account_name"),
                                    response.getString("account_gender"), response.getString("account_img"));

                            assetBookmark.insertUser(account);

                            try {
                                Thread.sleep(1500);
                                login();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            dismiss();
                        } else {
                            Toast.makeText(activity, "error", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(activity, error.toString(), Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };
            requestQueue.add(request);

        } else {
            Toast.makeText(activity, "Kiểm tra lại thông tin đăng ký...", Toast.LENGTH_LONG).show();
        }
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void show() {
        this.show(activity.getFragmentManager(), null);
    }

    ProgressDialog progressDialog;

    private boolean checkInput(String email, String pass) {
        if (email.isEmpty() || pass.isEmpty()
                || isValidEmailAddress(email.trim().toLowerCase()) == false)
            return false;
        return true;
    }

    public void login() {
        String txtEmail = email.getText().toString().trim().toLowerCase();
        String txtPass = pass.getText().toString().trim().toLowerCase();

        if (checkInput(txtEmail, txtPass) == false) {
            Toast.makeText(activity, "Kiểm tra lại thông tin đã nhập ....", Toast.LENGTH_LONG).show();
        } else {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Đang đăng nhập");
            progressDialog.show();
            StringRequest request = new StringRequest(Request.Method.GET, URL_DN + txtEmail + "/" + txtPass, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getInt("account_id") != 0) {

                            MainActivity.checkLogin = true;
                            Thread.sleep(1500);
                            dismiss();
                        } else {
                            Toast.makeText(activity, "error", Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(activity, "Kết nối mạng không ổn định", Toast.LENGTH_LONG).show();
                }
            });
            requestQueue.add(request);
        }
    }

    public ProgressDialog progress(String title) {
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(title);
        return progressDialog;
    }

    public void checkMailExists(String email) {
        final ProgressDialog progressDialog1 = progress("Đang kiểm tra mail...");
        progressDialog1.show();

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        StringRequest request = new StringRequest(Request.Method.GET, URL_DN1+email, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    if (response.equals("false")) {
                        progressDialog1.dismiss();
                        dangKi();
                    } else {
                        Toast.makeText(activity, "Mail đã tồn tại!", Toast.LENGTH_LONG).show();
                        progressDialog1.dismiss();
                    }
                } catch (Exception e) {
                    Toast.makeText(activity, "Mail đã tồn tại!", Toast.LENGTH_LONG).show();
                    progressDialog1.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(activity, "Kết nối mạng không ổn định!", Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(request);

    }
}
