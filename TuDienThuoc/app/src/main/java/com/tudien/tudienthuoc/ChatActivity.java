package com.tudien.tudienthuoc;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tudien.tudienthuoc.chat.ChatAdapter;
import com.tudien.tudienthuoc.chat.ChatMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

public class ChatActivity extends ActionBarActivity {

    Thread thread;

    private EditText messageET;
    private ListView messagesContainer;
    private Button sendBtn;
    private ChatAdapter adapter;

    private String key;
    private String nameClient;

    RequestQueue requestQueue;
    JsonObjectRequest request;

    static List<ChatMessage> chatMessagesInScreen = new ArrayList<>();

    final static String URL_create_mess = "http://192.168.1.5:8080/chat";
    final static String URL_get_mess = "http://192.168.1.5:8080/guitinnhan/";
    final static String URL_get_history_chat = "http://192.168.1.5:8080/lichsuchat/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        setTitle("Trò chuyện trực tuyến");

        thread = new Thread(new LoadingTask());

        // id account duoc truyen tu main_activity qua
        key = getIntent().getIntExtra("id", 0) + "";
        nameClient = getIntent().getStringExtra("name") + "";

        initControls();

        thread.start();
    }

    private class LoadingTask implements Runnable {

        @Override
        public void run() {
            while (true) {
                lay_tinnhan();
                try {
                    Thread.sleep(1000); // cu 1 s lai load du lieu 1 lan
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void initControls() {
        messagesContainer = (ListView) findViewById(R.id.messagesContainer);
        messageET = (EditText) findViewById(R.id.messageEdit);
        sendBtn = (Button) findViewById(R.id.chatSendButton);

        TextView meLabel = (TextView) findViewById(R.id.meLbl);
        TextView companionLabel = (TextView) findViewById(R.id.friendLabel);
        RelativeLayout container = (RelativeLayout) findViewById(R.id.container);
        companionLabel.setText("Bac si");// cho nay can set up cho 2 may,
        // may A bac si -> benh nhan thi set cho nay la benh nhan va nguoc lai

        requestQueue = Volley.newRequestQueue(ChatActivity.this);

        loadDummyHistory();

        lay_lich_su_chat();

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String messageText = messageET.getText().toString();

                if (TextUtils.isEmpty(messageText)) {
                    return;
                }

                // nguoi gui (phong4, userid =19, thoi gian gui, co phai nguoi do nhan khong)
                // nguoi gui (phong4, userid =15, thoi gian gui, co phai nguoi do nhan khong)(si)
                final ChatMessage chatMessage = new ChatMessage();
                chatMessage.setId(4);
                chatMessage.setMessage(messageText);
                chatMessage.setUserId(Long.parseLong(key));
                chatMessage.setDate(DateFormat.getDateTimeInstance().format(new Date()));
                chatMessage.setMe(true);

                tao_tinnhan(messageText);
                displayMessage(chatMessage);

                messageET.setText("");

            }
        });

    }

    // Neu tin nhan nao da xuat hien roi thi ko load tin nhan do nua
    public void displayMessage(ChatMessage message) {
        if (!chatMessagesInScreen.contains(message)) {
            chatMessagesInScreen.add(message);
            adapter.add(message);
            adapter.notifyDataSetChanged();
            scroll();
        }
    }

    private void scroll() {
        messagesContainer.setSelection(messagesContainer.getCount() - 1);
    }

    // lay tin nhan bac si
    private void loadDummyHistory() {

        adapter = new ChatAdapter(ChatActivity.this, new ArrayList<ChatMessage>());
        messagesContainer.setAdapter(adapter);
    }

    public void tao_tinnhan(String chatMessage) {

        final int room_id = 4;
        final int user_id = Integer.parseInt(key);

        Calendar c = Calendar.getInstance();
        final String room_time = c.get(Calendar.YEAR) + "-"
                + c.get(Calendar.MONTH) + "-"
                + c.get(Calendar.DAY_OF_MONTH)
                + " "
                + c.get(Calendar.HOUR_OF_DAY) + ":"
                + c.get(Calendar.MINUTE) + ":"
                + c.get(Calendar.SECOND);

        JSONObject js = new JSONObject();
        try {
            js.put("roomchat_id", room_id);
            js.put("roomchat_time", room_time);
            js.put("account_id", user_id);
            js.put("content_chat", chatMessage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request = new JsonObjectRequest(Request.Method.POST, URL_create_mess, js, new Response.Listener<JSONObject>() {

            public void onResponse(JSONObject response) {

                if (response != null) {

                } else {
                    Toast.makeText(ChatActivity.this, "error", Toast.LENGTH_LONG).show();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ChatActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        requestQueue.add(request);
    }

    public void lay_tinnhan() {

        StringRequest request = new StringRequest(Request.Method.GET, URL_get_mess + 4 + "/" + 15, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("account_id") != 0) {

                        ChatMessage msg = new ChatMessage();
                        msg.setId(19);
                        msg.setMe(false);
                        msg.setMessage(jsonObject.getString("content_chat"));
                        msg.setDate(jsonObject.getString("roomchat_time"));

                        displayMessage(msg);

                    } else {
                        Toast.makeText(ChatActivity.this, "error", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ChatActivity.this, "Kết nối mạng không ổn định", Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(request);
    }

    public void lay_lich_su_chat() {
        StringRequest request = new StringRequest(Request.Method.GET, URL_get_history_chat + 4 + "/" + 15 + "/" + 19, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray array = new JSONArray(response);// thu trâ ve la 1 mang cac json doan hoi thoai

                    if (array != null) {
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject row = array.getJSONObject(i); // ay tung json object ra

                            //acc chinh chu thi set true nhu code nay la account 19 la chinh nen true
                            if (row.getInt("account_id") == 19) {
                                ChatMessage msg = new ChatMessage();
                                msg.setId(19);
                                msg.setMe(true);
                                msg.setMessage(row.getString("content_chat"));
                                msg.setDate(row.getString("roomchat_time"));
                                displayMessage(msg);
                            } else {
                                ChatMessage msg = new ChatMessage();
                                msg.setId(15);
                                msg.setMe(false);
                                msg.setMessage(row.getString("content_chat"));
                                msg.setDate(row.getString("roomchat_time"));
                                displayMessage(msg);
                            }
                        }
                    } else {
                        Toast.makeText(ChatActivity.this, "Kết nối mạng không ổn định", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ChatActivity.this, "Kết nối mạng không ổn định", Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(request);
    }


}