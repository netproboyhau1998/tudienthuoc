package com.tudien.tudienthuoc.model;

public class Notes {
    private int id_notes;
    private int id_account;
    private String time_notes;
    private String content_notes;

    @Override
    public String toString() {
        return "Notes{" +
                "id_notes=" + id_notes +
                ", id_account=" + id_account +
                ", time_notes='" + time_notes + '\'' +
                ", content_notes='" + content_notes + '\'' +
                '}';
    }

    public int getId_notes() {
        return id_notes;
    }

    public void setId_notes(int id_notes) {
        this.id_notes = id_notes;
    }

    public int getId_account() {
        return id_account;
    }

    public void setId_account(int id_account) {
        this.id_account = id_account;
    }

    public String getTime_notes() {
        return time_notes;
    }

    public void setTime_notes(String time_notes) {
        this.time_notes = time_notes;
    }

    public String getContent_notes() {
        return content_notes;
    }

    public void setContent_notes(String content_notes) {
        this.content_notes = content_notes;
    }
}
